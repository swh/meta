/title Ingest <stack>

/milestone %"Extend archive coverage [Roadmap - Collect]"

Summary:

<Replace this by a small summary or context for the new stack>

Plan:
- [ ] [Implement lister](https://docs.softwareheritage.org/devel/swh-lister/tutorial.html)
- [ ] [Implement loader](https://docs.softwareheritage.org/devel/swh-loader-core/package-loader-tutorial.html)
- [ ] docker: [Run lister](https://docs.softwareheritage.org/devel/swh-lister/run_a_new_lister.html)
- [ ] docker: Run loader
- [ ] Document lister
- [ ] [Document loader](https://docs.softwareheritage.org/devel/swh-loader-core/package-loader-specifications.html)
- [ ] Deploy on staging
- [ ] Call for public review
- [ ] Deploy on production
