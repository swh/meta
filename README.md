Meta
----

Meta repository for the Meta project.

It's not a repository of source code. It's a repository to allow creation of gitlab
issue templates.
